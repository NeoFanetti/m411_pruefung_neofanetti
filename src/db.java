import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

/**
 * Datenbankfunktionen
 * 
 * @author D. Mosimann
 */
public class db {
    private Connection conn;
    
    public db() {
        String  baseurl = "jdbc:mysql://localhost/m411nr";     
        String  user = "root";
        String  password = "";        
        
        try {
            conn = DriverManager.getConnection(baseurl, user, password);
        } catch (Exception e) {
            System.err.println(e);
            System.exit(0);
        }
    }
    
    public ArrayList<String> selectName() {
        ArrayList<String> nlist = new ArrayList();
        Statement sqlStatement;
        ResultSet result;

        try {
            sqlStatement = conn.createStatement();
            sqlStatement.execute("SELECT * FROM NAME"); 
            result = sqlStatement.getResultSet(); 
            while (result.next()) {
                nlist.add( result.getString("name").toLowerCase() );
            }
        } catch (Exception e) {
            System.out.println(e);
        }
       
        return nlist;
    }
    
    public ArrayList<String> selectVorname() {
        ArrayList<String> nlist = new ArrayList();
        Statement sqlStatement;
        ResultSet result;

        try {
            sqlStatement = conn.createStatement();
            sqlStatement.execute("SELECT * FROM VORNAME"); 
            result = sqlStatement.getResultSet(); 
            while (result.next()) {
                nlist.add( result.getString("vorname").toLowerCase() );
            }
        } catch (Exception e) {
            System.out.println(e);
        }
       
        return nlist;
    }
    
    // Aufgabe 2
    public ArrayList<String> selectFarben() {
        ArrayList<String> flist = new ArrayList();
        Statement sqlStatement;
        ResultSet result;
        
        try {
            sqlStatement = conn.createStatement();
            sqlStatement.execute("SELECT * FROM FARBE");
            result = sqlStatement.getResultSet();
            while (result.next()) {
                flist.add( result.getString("farbe").toLowerCase() );
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return flist;
    }
    
    //Aufgabe 4
    public void insertRandom() {
        Statement sqlStatement;
        ResultSet result;
        // Random Number (pid)
        int randomPid = (int)(Math.random() * 1000 + 1);
        int randomFid = (int)(Math.random() * 1000 + 1);
        Random randomName = new Random();
        char randomizedName = (char) (randomName.nextInt(26) + 'a');
        Random randomVorname = new Random();
        char randomizedVorname = (char) (randomVorname.nextInt(26) + 'a');
        
        
        try {
            sqlStatement = conn.createStatement();
            sqlStatement.executeUpdate("INSERT INTO personen " + "VALUES ("+randomPid+", "+randomizedName+", "+randomizedVorname+", "+randomFid+")");
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
}
